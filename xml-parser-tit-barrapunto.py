#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

class myContentHandler(ContentHandler):

    def __init__(self):
        self.inItem= False
        self.inContent = False
        self.theContent = ""

    def startElement(self, name, attrs): #etiqueta de inicio
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True

    def endElement(self, name):  # etiqueta de fin
        if name == 'item':
            self.inItem = False
            print("<br><a href='" + self.link + "'>" + self.title + "</a>")
        elif self.inItem:
            if name == 'title':
                self.title = self.theContent
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                self.link = self.theContent
                self.inContent = False
                self.theContent = ""

    def characters(self, chars): #caracteres a almacenar
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog

if len(sys.argv) < 1:
    print("Usage: python xml-parser-titulares-barrapunto.py > fichero.html")
    print()
    print(" <document>: file name of the document to parse")
    sys.exit(1)

# Load parser and driver

theParser = make_parser()
theHandler = myContentHandler()
theParser.setContentHandler(theHandler)

# Ready, set, go!

print("<html><head><body><h1>Noticias</h1>")
xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')  #recupera el rss de barrapunto
theParser.parse(xmlFile)
print("</body></head></html>")
